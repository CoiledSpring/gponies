This is a project made to use the Derpibooru API.
It is split in two parts : 
 - libpony
 - gponies
Libpony is the back-end of gponies. It uses CURL to fetch data from Derpibooru, Json-C to parse the received data, and GLib for the GData type, and possibly others types in the future.
Gponies is the GUI, which is not yet begun. It will use GTK+, and obviously libpony.

Since it is free (as in freedom), you can use the code for your own project (if you do, I'd be thankful if you credit me). You can also use libpony if you want to make another front-end to it.

