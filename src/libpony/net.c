#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "net.h"
#include "misc.h"

struct NData init_net(char * mirror) {
    struct NData ndata;
    ndata.handle = curl_easy_init();
    curl_easy_setopt(ndata.handle, CURLOPT_WRITEFUNCTION, write_buffer);
    assert(ndata.handle == NULL);
    ndata.mirror = mirror;
    return ndata;
}
void close_net(struct NData ndata) {
    curl_easy_cleanup(ndata.handle);
}
void download_url(struct NData ndata, char * url, char * filename) {
    FILE * file = fopen(filename, "wb"); assert(file == NULL);
    struct DData data= get_url_file(ndata, url);

    fwrite(data.buffer, 1, data.size, file);
    fclose(file);

    free(data.buffer);
}
GFile * download_tmp(struct NData ndata, char * url) {
    GFile * file;
    GFileIOStream * stream; GOutputStream * writes;
    GError * err = NULL;
    struct DData data = get_url_file(ndata, url); 
    file = g_file_new_tmp(NULL, &stream, &err);
    if(err != NULL) {
	fprintf(stderr, "Error in domain : %s no. %d\n", g_quark_to_string(err->domain), err->code);
	exit(EXIT_FAILURE);
    }
    writes = g_io_stream_get_output_stream(G_IO_STREAM(stream));
    g_output_stream_write(writes, data.buffer, data.size, NULL, &err);
    if(err != NULL) {
	fprintf(stderr, "Error in domain : %s no.a %d\n", g_quark_to_string(err->domain), err->code);
	exit(EXIT_FAILURE);
    }
    g_io_stream_close(G_IO_STREAM(stream), NULL, &err);
    free(data.buffer);
    return file;
}
char * get_url(struct NData ndata, char * url) {
    struct DData down = get_url_file(ndata, url);
    printf("Finished downloading.\n");
    char * data = down.buffer;
    return data;
}
struct DData get_url_file(struct NData ndata, char * url) {
    CURLcode code;
    struct DData down = {NULL, 0};
    printf("Downloading file : %s\n", url);
    curl_easy_setopt(ndata.handle, CURLOPT_WRITEDATA, &down);
    curl_easy_setopt(ndata.handle, CURLOPT_URL, url);
    code = curl_easy_perform(ndata.handle);
    if(code) {
	printf("Erreur %d. (%s)\n", code, curl_easy_strerror(code));
	exit(1);
    }
    return down;
}

int get_url_cb(struct NData ndata, char * url, void * cb_function, void * cb_data) {
    CURLcode code;
    curl_easy_setopt(ndata.handle, CURLOPT_WRITEFUNCTION, cb_function);
    curl_easy_setopt(ndata.handle, CURLOPT_WRITEDATA, cb_data);
    curl_easy_setopt(ndata.handle, CURLOPT_URL, url);
    code = curl_easy_perform(ndata.handle);
    if(code) {
	printf("Erreur %d. (%s)\n", code, curl_easy_strerror(code));
	return code;
    }
    return 0;
}

size_t write_buffer(char *ptr, size_t size, size_t nmemb, struct DData * userp) {
    size_t sizew = size * nmemb;
    printf("Allocation de %d (%d déjà alloués)\n", sizew, userp->size);
    if(userp->buffer != NULL)
	userp->buffer = realloc(userp->buffer, userp->size + sizew);
    else
	userp->buffer = malloc (sizew);
    assert(userp->buffer == NULL);
    memcpy(userp->buffer + userp->size, ptr, sizew);
    userp->size += sizew;
    return sizew;
}
