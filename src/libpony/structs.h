#ifndef STRUCTS_H
#define STRUCTS_H
#include <time.h>
#include <glib.h>
struct Image {
    char id [25];
    int id_number;
    struct tm created_at;
    struct tm updated_at;
    char * description;
    char *  uploader;
    int upvotes;
    int downvotes;
    int faves;
    int comment_count;
    char * tags;
    int nb_ids;
    char ** tag_ids;
    int width;
    int height;
    char * format;
    char * mime_type;
    char * source;
    char * license;
    GData * images;
};
struct Image_Array {
    struct Image * images;
    int length;
};
struct Search {
    struct Image_Array array;
    int total;
};
#endif /* STRUCTS_H */
