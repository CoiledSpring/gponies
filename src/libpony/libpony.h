#ifndef LIBPONY_H
#define LIBPONY_H
#include <time.h>
#include <stdbool.h>
#include <curl/curl.h>
#include <json-c/json.h>
#include <glib.h>
#include <gio/gio.h>
/* Net */
struct NData {
    CURL * handle;
    char * mirror;
};
struct DData {
    char * buffer;
    size_t size;
};
struct NData init_net(char * mirror);
int close_net(struct NData ndata);
char * get_url(struct NData ndata, char * url);
struct DData get_url_file(struct NData ndata, char * url);
int get_url_cb(struct NData ndata, char * url, void * cb_function, void * cb_data);
void download_url(struct NData ndata, char * url, char * filename);
GFile * download_tmp(struct NData ndata, char * url);
/* Includes */
#include "libderpy.h"
#endif /* LIBPONY_H */
