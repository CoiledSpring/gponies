#ifndef MISC_H
#define MISC_H
#include <libintl.h>
#include <locale.h>
#define assert(x) if(x) { fprintf(stderr, _("Error at line %d, in file %s\n"), __LINE__, __FILE__); exit(EXIT_FAILURE); }
#define _(s) gettext(s)

#endif /* MISC_H */
