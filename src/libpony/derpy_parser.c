#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <json-c/json.h>
#include "structs.h"
#include "derpy_parser.h"
#include "misc.h"
struct tm _parse_date(char * date) {
    struct tm time;
    sscanf(date,
	   "%d-%d-%d T %d:%d:%d",
	   &time.tm_year,
	   &time.tm_mday,
	   &time.tm_mon,
	   &time.tm_hour,
	   &time.tm_min,
	   &time.tm_sec);
    time.tm_year -= 1900;
    return time;
}
struct Image derpy_parse_json_image(char * json) {
    json_object * j = json_tokener_parse(json);
    return derpy_parse_json_tokened_image(j);
}
struct Image derpy_parse_json_tokened_image(json_object * j) {
    struct Image image;
    json_object * obj, * ele; char * tmp; int i;

    /* ID */
    assert(json_object_object_get_ex(j, "id", &obj) == 0);
    assert(json_object_is_type(obj, json_type_string)==0);
    tmp = (char *)json_object_get_string(obj);
    strcpy(image.id, tmp);
    free(tmp);

    /* ID Number */
    assert(json_object_object_get_ex(j, "id_number", &obj) == 0);
    assert(json_object_is_type(obj, json_type_int)         == 0);
    image.id_number = json_object_get_int(obj);

    /* Date of creation */
    assert(json_object_object_get_ex(j, "created_at", &obj) == 0);
    assert(json_object_is_type(obj, json_type_string)       == 0);
    tmp = (char *)json_object_get_string(obj);
    image.created_at = _parse_date(tmp);
    free(tmp);

    /* Date of update */
    assert(json_object_object_get_ex(j, "updated_at", &obj) == 0);
    assert(json_object_is_type(obj, json_type_string)       == 0);
    tmp = (char *)json_object_get_string(obj);
    image.updated_at = _parse_date(tmp);
    free(tmp);

    /* Description */
    assert(json_object_object_get_ex(j, "description", &obj) == 0);
    assert(json_object_is_type(obj, json_type_string)        == 0);
    image.description = (char *)json_object_get_string(obj);

    /* Uploader */
    assert(json_object_object_get_ex(j, "uploader", &obj) == 0);
    assert(json_object_is_type(obj, json_type_string)     == 0);
    image.uploader = (char *)json_object_get_string(obj);

    /* Upvotes & Downvotes */
    assert(json_object_object_get_ex(j, "upvotes", &obj) == 0);
    assert(json_object_is_type(obj, json_type_int)       == 0);
    image.upvotes = json_object_get_int(obj);
    assert(json_object_object_get_ex(j, "downvotes", &obj) == 0);
    assert(json_object_is_type(obj, json_type_int)         == 0);
    image.downvotes = json_object_get_int(obj);

    /* Faves */
    assert(json_object_object_get_ex(j, "faves", &obj) == 0);
    assert(json_object_is_type(obj, json_type_int)     == 0);
    image.faves = json_object_get_int(obj);

    /* Comment Count */
    assert(json_object_object_get_ex(j, "comment_count", &obj) == 0);
    assert(json_object_is_type(obj, json_type_int)             == 0);
    image.comment_count = json_object_get_int(obj);

    /* Tags */
    assert(json_object_object_get_ex(j, "tags", &obj) == 0);
    assert(json_object_is_type(obj, json_type_string) == 0);
    image.tags = (char *)json_object_get_string(obj);

    /* Tag List */
    assert(json_object_object_get_ex(j, "tag_ids", &obj) == 0);
    assert(json_object_is_type(obj, json_type_array)     == 0);
    image.nb_ids  = json_object_array_length(obj);
    image.tag_ids = malloc(image.nb_ids * sizeof(char *));
    assert(image.tag_ids == NULL);
    for(i = 0; i < image.nb_ids; i++) {
	ele = json_object_array_get_idx(obj, i);
	assert(json_object_is_type(ele, json_type_string) == 0);
	image.tag_ids[i] = (char *)json_object_get_string(ele);
    }

    /* Width & Height */
    assert(json_object_object_get_ex(j, "width", &obj) == 0);
    assert(json_object_is_type(obj, json_type_int)     == 0);
    image.width = json_object_get_int(obj);
    assert(json_object_object_get_ex(j, "height", &obj) == 0);
    assert(json_object_is_type(obj, json_type_int)      == 0);
    image.height = json_object_get_int(obj);

    /* Format & Mime Type */
    assert(json_object_object_get_ex(j, "original_format", &obj) == 0);
    assert(json_object_is_type(obj, json_type_string)            == 0);
    image.format = (char *)json_object_get_string(obj);
    assert(json_object_object_get_ex(j, "mime_type", &obj) == 0);
    assert(json_object_is_type(obj, json_type_string)      == 0);
    image.mime_type = (char *)json_object_get_string(obj);

    /* Source & License */
    assert(json_object_object_get_ex(j, "source_url", &obj) == 0);
    if(!json_object_is_type(obj, json_type_string)) {
	image.source = (char *)json_object_get_string(obj);
    } else {
	image.source = NULL;
    }
    assert(json_object_object_get_ex(j, "license", &obj) == 0);
    if(!json_object_is_type(obj, json_type_string)) {
	image.license = (char *)json_object_get_string(obj);
    } else {
	image.license = NULL;
    }

    /* Images URL */
    g_datalist_init(&image.images);
    assert(json_object_object_get_ex(j, "representations", &obj) == 0);
    assert(json_object_is_type(obj, json_type_object)            == 0);
    json_object_object_foreach(obj, key, val) {
	assert(json_object_is_type(val, json_type_string) == 0);
	g_datalist_set_data(&image.images, key, (gpointer)json_object_get_string(val));
    }
    return image;
}

struct Search derpy_parse_json_search(char * json) {
    json_object * j = json_tokener_parse(json);
    return derpy_parse_json_tokened_search(j);
}

struct Search derpy_parse_json_tokened_search(json_object * j) {
    struct Search search;
    json_object * obj;

    /* Total */
    assert(json_object_object_get_ex(j, "total", &obj) == 0);
    assert(json_object_is_type(obj, json_type_int)     == 0);
    search.total = json_object_get_int(obj);

    /* The fun part :D */
    assert(json_object_object_get_ex(j, "search", &obj) == 0);
    search.array = derpy_parse_json_tokened_image_array(obj);
    
    return search;
}
struct Image_Array derpy_parse_json_image_array(char * json) {
    json_object * j = json_tokener_parse(json);
    return derpy_parse_json_tokened_image_array(j);
}
struct Image_Array derpy_parse_json_tokened_image_array(json_object * j) {
    struct Image_Array array; int i;
    json_object * ele;
    assert(json_object_is_type(j, json_type_array) == 0);
    array.length = json_object_array_length(j); if(array.length == 0) { array.images = NULL; return array; }
    array.images = malloc(array.length * sizeof(struct Image));
    assert(array.images == NULL);
    
    for(i = 0; i < array.length; i++) {
	ele = json_object_array_get_idx(j, i);
	assert(json_object_is_type(ele, json_type_object) == 0);
	array.images[i] = derpy_parse_json_tokened_image(ele);
    }

    return array;
}
