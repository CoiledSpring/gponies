#ifndef DERPY_NET_H
#define DERPY_NET_H
char * derpy_get_image_json(struct NData ndata, int id);
char * derpy_get_search_json(struct NData ndata, char * q, int page, bool comments, bool fav);
char * derpy_get_image_list_json(struct NData ndata, char * constraint, int page, int gt, int gte, int lt, int lte, char order, bool deleted, bool comments, bool fav);
#endif /* DERPY_NET_H */
