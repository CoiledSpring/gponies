#ifndef NET_H
#define NET_H
#include <stdbool.h>
#include <glib.h>
#include <gio/gio.h>
#include <curl/curl.h>
struct NData {
    CURL * handle;
    char * mirror;
};
struct DData {
    char * buffer;
    size_t size;
};
struct NData init_net(char * mirror);
void close_net(struct NData ndata);
char * get_url(struct NData ndata, char * url);
struct DData get_url_file(struct NData ndata, char * url);
int get_url_cb(struct NData ndata, char * url, void * cb_function, void * cb_data);
void download_url(struct NData ndata, char * url, char * filename);
GFile * download_tmp(struct NData ndata, char * url);
size_t write_buffer(char *ptr, size_t size, size_t nmemb, struct DData * userp);
#endif /* NET_H */
