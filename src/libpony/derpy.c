#include <stdio.h>
#include <stdlib.h>
#include <json-c/json.h>
#include "misc.h"
#include "libpony.h"
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
struct Image derpy_get_image(struct NData ndata, int id) {
    struct Image image; char * json;
    json = derpy_get_image_json(ndata, id);
    image = derpy_parse_json_image(json);
    free(json);
    return image;
}

struct Search derpy_get_search(struct NData ndata, char * query, int page) {
    struct Search search; char * json;
    json = derpy_get_search_json(ndata, query, page, false, false);
    printf("Finished downloading JSON.\n");
    search = derpy_parse_json_search(json);
    printf("Finished parsing JSON.\n");
    free(json);
    return search;
}
void derpy_free_image(struct Image image) {
    printf("Image %d freed.\n", image.id_number);
    free(image.description);
    free(image.uploader);
    free(image.tags);
    image.nb_tags--;
    while(image.nb_tags--) {
	free(image.tag_ids[image.nb_tags]);
    }
    free(image.format);
    free(image.mime_type);
    free(image.source);
    free(image.license);
    g_datalist_clear (&image.images);
}
void derpy_free_array(struct Image_Array array) {
    while(array.length) {
	array.length--;
	derpy_free_image(array.images[array.length]);
    }
    free(array.images);
}
void derpy_free_search(struct Search search) {
    derpy_free_array(search.array);
}
