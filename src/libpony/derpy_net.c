#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "net.h"
#include "misc.h"
#include "derpy_net.h"
char * derpy_get_image_json(struct NData ndata, int id) {
    char * s = malloc(100), * data;
    assert(s == NULL);
    sprintf(s, "%s/%d.json", ndata.mirror, id);
    data = get_url(ndata, s);
    free(s);
    return data;
}
char * derpy_get_search_json(struct NData ndata, char * q, int page, bool comments, bool fav) {
    char * s = malloc(100), * data;
    assert(s == NULL);
    sprintf(s, "%s/search.json?q=%s&page=%d&comments=%d&fav=%d", ndata.mirror, q, page, comments, fav);
    data = get_url(ndata, s);
    free(s);
    return data;
}
char * derpy_get_image_list_json(struct NData ndata, char * constraint, int page, int gt, int gte, int lt, int lte, char order, bool deleted, bool comments, bool fav) {
    char * s = malloc(200), * data;
    assert(s == NULL);
    sprintf(s, "%s/images.json?", ndata.mirror);
    if(constraint) sprintf(s + strlen(s), "constraint=%s&", constraint);
    if(page)       sprintf(s + strlen(s), "page=%d&", page);
    if(gt)         sprintf(s + strlen(s), "gt=%d&", gt);
    if(gte)        sprintf(s + strlen(s), "gte=%d&", gte);
    if(lt)         sprintf(s + strlen(s), "lt=%d&", lt);
    if(lte)        sprintf(s + strlen(s), "lte=%d&", lte);
    if(order)      sprintf(s + strlen(s), "order=%c", order);
    if(deleted)    sprintf(s + strlen(s), "deleted=%d", deleted);
    if(comments)   sprintf(s + strlen(s), "comments=%d",comments);
    if(fav)        sprintf(s + strlen(s), "fav=%d", fav);
    data = get_url(ndata, s);
    free(s);
    return data;
}
