#ifndef LIBDERPY_H
#define LIBDERPY_H
struct Image {
    char id [25];
    int id_number;
    struct tm created_at;
    struct tm updated_at;
    char * description;
    char *  uploader;
    int upvotes;
    int downvotes;
    int faves;
    int comment_count;
    char * tags;
    int nb_tags;
    char ** tag_ids;
    int width;
    int height;
    char * format;
    char * mime_type;
    char * source;
    char * license;
    GData * images;
};
struct Image_Array {
    struct Image * images;
    int length;
};
struct Search {
    struct Image_Array array;
    int total;
};
/* Net functions */
#include "derpy_net.h"
/* Parse functions */
#include "derpy_parser.h"
/* Front end functions */
struct Image derpy_get_image(struct NData ndata, int id);
struct Search derpy_get_search(struct NData ndata, char * query, int page);
void derpy_free_image(struct Image);
void derpy_free_search(struct Search);
#endif /* LIBDERPY_H */
