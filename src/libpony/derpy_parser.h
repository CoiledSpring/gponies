#ifndef PARSER_H
#define PARSER_H
struct Image derpy_parse_json_image(char * json);
struct Image derpy_parse_json_tokened_image(json_object * j);
struct Search derpy_parse_json_search(char * json);
struct Search derpy_parse_json_tokened_search(json_object * j);
struct Image_Array derpy_parse_json_image_array(char * json);
struct Image_Array derpy_parse_json_tokened_image_array(json_object * j);
#endif /* PARSER_H */
