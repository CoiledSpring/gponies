#ifndef STRUCTS_H
#define STRUCTS_H
#include <time.h>
#include <glib.h>
struct Search {
    GArray images;
    int total;
};
struct Image {
    char id [25];
    int id_number;
    time_t created_at;
    time_t updated_at;
    GString description;
    GString uploader;
    int upvotes;
    int downvotes;
    int faves;
    int comment_count;
    GString tags;
    GStringChunk * tag_ids;
    int width;
    int height;
    GString format;
    GString mime_type;
    GString source;
    GString licence;
    GData * images;
};
#endif /* STRUCTS_H */
