#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <libpony.h>
void help() {
    printf("Use : ponies-cli [-d|--download ID] [-v|--verbose]\n");
    exit(EXIT_SUCCESS);
}

int get_id(int * argc, char *** argv) {
    int id = 0; char * err;
    if((*argc) > 2) {
	id = strtol((*argv)[2], &err, 10);
	if((*err) != '\0') {
	    printf("An integer was expected for argument to [-d | --download].\n");
	    exit(EXIT_FAILURE);
	}
	(*argc)--; (*argv)++;
    } else {
	fprintf(stderr, "Error ! -d requires an argument.\n");
	exit(EXIT_FAILURE);
    }
    return id;
}
int main(int argc, char ** argv) {
    bool verbose = false;
    int id = -1;
    FILE * file = NULL;
    struct NData ndata = init_net("https://www.derpibooru.org");
    struct Image image;
    char * filename = malloc(100);
    char * url = malloc(100);
    
    while((argc > 1) && (argv[1][0] == '-')) {
	switch(argv[1][1]) {
	case '-':
	    if(strcmp(&argv[1][2], "help") == 0) {
		help();
	    } else if(strcmp(&argv[1][2], "verbose") == 0) {
		verbose = true;
	    } else if(strcmp(&argv[1][2], "download") == 0) {
		id = get_id(&argc, &argv);
	    }
	    break;
	case 'h':
	    help();
	    break;
	case 'v':
	    verbose = true;
	    break;
	case 'd':
	    id = get_id(&argc, &argv);
	    break;
	default:
	    fprintf(stderr, "Unknown argument : %c\n", argv[1][1]); exit(EXIT_FAILURE);
	}
	argc--;
	argv++;
    }
    if(id != -1) {
	image = derpy_get_image(ndata, id);
	sprintf(filename, "%d.%s", id, image.format);
	sprintf(url, "https:%s", (char *)g_datalist_get_data(&image.images, "full"));
        download_url(ndata, url, filename);
    }

    return 0;
}
