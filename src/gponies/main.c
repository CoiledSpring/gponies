#include <stdio.h>
#include <stdlib.h>
#include <libpony.h>
#include <gtk/gtk.h>
#include "search.h"
struct NData global_net;
int main(int argc, char ** argv) {
    GObject *window, *scroll, * icon_view, * search_widget;
    GtkBuilder * builder;
    GtkListStore *list_store;
    
    gtk_init (&argc, &argv);
    global_net = init_net("https://www.derpibooru.org");
    
    builder = gtk_builder_new_from_file("src/gponies/gponies.ui");
    
    window = gtk_builder_get_object(builder, "window1");
    gtk_window_set_title (GTK_WINDOW (window), "GPonies");

    scroll = gtk_builder_get_object(builder, "scrolledwindow1");
    gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scroll), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
    gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (scroll), GTK_SHADOW_IN);
  
    icon_view = gtk_builder_get_object(builder, "iconview1");
    list_store = gtk_list_store_new (1, GDK_TYPE_PIXBUF);
    gtk_icon_view_set_model(GTK_ICON_VIEW(icon_view), GTK_TREE_MODEL(list_store));
    gtk_icon_view_set_pixbuf_column (GTK_ICON_VIEW (icon_view), 0);
    
    search_widget = gtk_builder_get_object(builder, "searchentry1");

    g_signal_connect (GTK_WIDGET(search_widget), "search-changed", G_CALLBACK(cb_search), icon_view);
    g_signal_connect (GTK_WIDGET(window), "destroy", G_CALLBACK (gtk_main_quit), NULL);
    
    gtk_widget_show_all(GTK_WIDGET(scroll));
    gtk_widget_show(GTK_WIDGET(window));
    gtk_main ();
}
