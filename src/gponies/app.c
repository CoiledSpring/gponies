#include <gtk/gtk.h>
#include "app.h"
#include "appwin.h"
struct _GPonies
{
  GtkApplication parent;
};

struct _GPoniesClass
{
  GtkApplicationClass parent_class;
};

G_DEFINE_TYPE(GPonies, gponies, GTK_TYPE_APPLICATION);

static void gponies_init (GPonies *app) {
}

static void gponies_activate (GApplication *app) {
  GPoniesWindow *win;

  win = gponies_window_new (GPONIES (app));
  gtk_window_present (GTK_WINDOW (win));
}

static void gponies_open (GApplication  *app,
			  GFile        **files,
			  gint           n_files,
			  const gchar   *hint) {
  GList *windows;
  GPoniesWindow *win;
  int i;

  windows = gtk_application_get_windows (GTK_APPLICATION (app));
  if (windows)
    win = GPONIES_WINDOW (windows->data);
  else
    win = gponies_window_new (GPONIES (app));

  for (i = 0; i < n_files; i++)
    gponies_window_open (win, files[i]);

  gtk_window_present (GTK_WINDOW (win));
}

static void gponies_class_init (GPoniesClass *class) {
  G_APPLICATION_CLASS (class)->activate = gponies_activate;
  G_APPLICATION_CLASS (class)->open = gponies_open;
}

GPonies * gponies_new (void) {
  return g_object_new (GPONIES_TYPE,
                       "application-id", "org.gtk.gponies",
                       "flags", G_APPLICATION_HANDLES_OPEN,
                       NULL);
}

