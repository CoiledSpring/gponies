#ifndef APP_H
#define APP_H
#include <gtk/gtk.h>

#define GPONIES_TYPE (gponies_get_type ())
#define GPONIES(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), GPONIES_TYPE, GPonies))


typedef struct _GPonies       GPonies;
typedef struct _GPoniesClass  GPoniesClass;


GType           gponies_get_type    (void);
GPonies        *gponies_new         (void);

#endif /* APP_H */
