#include <gtk/gtk.h>
#include <glib.h>
#include <string.h>
#include <libpony.h>
#include "search.h"
GThread * thread = NULL;
struct Thread_Data {
    GCancellable * cancel;
    char * url;
    char * name;
    GtkListStore * list;
    GtkTreeIter * iter;
};
void make_directory_tmp() {
    GFile * folder;
    char * path = malloc(30);
    sprintf(path, "%s/derpibooru/", g_get_tmp_dir());
    folder = g_file_new_for_path(path);
    g_file_make_directory(folder, NULL, NULL);
    free(path);
}
GFile * download_image(struct NData ndata, char * url, char * name) { /* If image is in cache, fetch it. If not, download it. */
    GFile * file;
    char * filename = malloc(100);
    GFileOutputStream * stream;
    struct DData data;

    make_directory_tmp();
    sprintf(filename, "%s/derpibooru/%s", g_get_tmp_dir(), name);
    file = g_file_new_for_path(filename);
    if(!g_file_query_exists(file, NULL)) {
	printf("Le fichier n'existe pas.\n");
	stream = g_file_create(file, G_FILE_CREATE_NONE, NULL, NULL);
	data = get_url_file(ndata, url);
	g_output_stream_write(G_OUTPUT_STREAM(stream), data.buffer, data.size, NULL, NULL);
	g_output_stream_close(G_OUTPUT_STREAM(stream), NULL, NULL);
	free(data.buffer);
    } else printf("Le fichier existe.\n");
    
    free(filename);
    return file;
}

void add_image(GFileInputStream * stream, GCancellable * cancel, GtkListStore * list_store, GtkTreeIter * iter)
{
    GdkPixbuf * pix;
    pix = gdk_pixbuf_new_from_stream(G_INPUT_STREAM(stream), NULL, NULL);
    gtk_list_store_set (list_store, iter, 0, pix, -1);
    free(iter);
}

gpointer thread_image(gpointer data) {
    GFile * file;
    GFileInputStream * stream;
    struct NData ndata = init_net("https://derpibooru.org");
    
    file   = download_image(ndata, ((struct Thread_Data *)data)->url, ((struct Thread_Data *)data)->name);
    stream = g_file_read(file, NULL, NULL);
    add_image(stream, ((struct Thread_Data *)data)->cancel, ((struct Thread_Data *)data)->list, ((struct Thread_Data *)data)->iter);
    
    g_object_unref(stream);
    g_object_unref(file);
    
    close_net(ndata);
    free(((struct Thread_Data *)data)->url);
    free(((struct Thread_Data *)data)->name);
    free((struct Thread_Data *)data);
    return NULL;
}

void cb_search(GtkSearchEntry *entry, GtkWidget * icon_view)
{
    struct Search search;
    GtkListStore * list = GTK_LIST_STORE(gtk_icon_view_get_model(GTK_ICON_VIEW(icon_view)));
    char * text = (char *)gtk_entry_get_text (GTK_ENTRY(entry));
    struct Thread_Data * data;
    int i;
    
    if(*text != 0) {
	gtk_list_store_clear(list);
	printf("Searching %s …\n", text);
        search = derpy_get_search(global_net, text, 0);
        for(i = 0; i < search.array.length; i ++) {
	    data = malloc(sizeof(struct Thread_Data));
	    data->iter = malloc(sizeof(GtkTreeIter));
	    data->url    = malloc(100);
	    data->name   = malloc (10);
	    data->cancel = g_cancellable_new();
	    data->list   = list;
	    gtk_list_store_append(list, data->iter);
	    sprintf(data->url, "https:%s", (char *)g_datalist_get_data(&search.array.images[i].images, "thumb"));
	    sprintf(data->name,"%d", search.array.images[i].id_number);
	    g_thread_new("Image", thread_image, data);
	}
    }
    
    derpy_free_search(search);
}
