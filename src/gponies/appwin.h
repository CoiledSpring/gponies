#ifndef APPWIN_H
#define APPWIN_H
#include <gtk/gtk.h>
#include "app.h"

#define GPONIES_WINDOW_TYPE (gponies_window_get_type ())
#define GPONIES_WINDOW(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), GPONIES_WINDOW_TYPE, GPoniesWindow))


typedef struct _GPoniesWindow         GPoniesWindow;
typedef struct _GPoniesWindowClass    GPoniesWindowClass;


GType                   gponies_window_get_type     (void);
GPoniesWindow          *gponies_window_new          (GPonies *app);
void                    gponies_window_open         (GPoniesWindow *win,
						     GFile            *file);
#endif /* APPWIN_H */
