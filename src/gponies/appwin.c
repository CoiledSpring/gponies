#include <gtk/gtk.h>

#include "app.h"
#include "appwin.h"

struct _GPoniesWindow {
    GtkApplicationWindow parent;
};

struct _GPoniesWindowClass {
    GtkApplicationWindowClass parent_class;
};

G_DEFINE_TYPE(GPoniesWindow, gponies_window, GTK_TYPE_APPLICATION_WINDOW);

static void gponies_window_init (GPoniesWindow *app) {

}

static void gponies_window_class_init (GPoniesWindowClass *class) {

}

GPoniesWindow * gponies_window_new (GPonies *app) {
  return g_object_new (GPONIES_WINDOW_TYPE, "application", app, NULL);
}

void
gponies_window_open (GPoniesWindow *win,
		     GFile            *file) {
}

